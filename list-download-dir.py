import os
import subprocess 

def get_download_dir_with_os_open():
    stream = os.popen('xdg-user-dir DOWNLOAD')
    output = stream.read()
    return output

def get_download_dir():
    process = subprocess.run(['xdg-user-dir','DOWNLOAD'], 
                             stdout=subprocess.PIPE, 
                             universal_newlines=True)
    return process.stdout.strip()


def list_files_from_dir(dir):
    print('Buscando archivos en ',dir)
    process = subprocess.run(['ls',dir], 
                             stdout=subprocess.PIPE, 
                             universal_newlines=True)                         
    return process.stdout

download_dir = get_download_dir()
list_files = list_files_from_dir(download_dir)
print("\nDirectorio: ",download_dir,"\n")
for archivo in list_files.split('\n'):
    print(archivo.strip())